package sourceit;

public class Generator {
    public static Bank[] generate() {
        Bank[] banks = new Bank[3];

        banks[0] = new Bank("Приват", 26.050f, 26.350f, 32.100f, 32.500f, 44.80f, 47.00f);
        banks[1] = new Bank("Ощад", 27.050f, 27.350f, 33.100f, 33.500f, 45.80f, 48.00f);
        banks[2] = new Bank("ПУМБ", 28.050f, 28.350f, 34.100f, 34.500f, 46.80f, 49.00f);
        return banks;
    }
}

